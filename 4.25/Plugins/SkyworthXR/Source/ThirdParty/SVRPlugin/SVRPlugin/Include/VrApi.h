/************************************************************************************

Filename    :   VrApi.h
Content     :   Minimum necessary API for mobile VR
Created     :   June 25, 2014
Authors     :   John Carmack, J.M.P. van Waveren
Language    :   C99

Copyright   :   Copyright (c) Facebook Technologies, LLC and its affiliates. All rights reserved.

*************************************************************************************/
#ifndef OVR_VrApi_h
#define OVR_VrApi_h

#if defined(__cplusplus)
extern "C" {
#endif
void vrapi_GLDebug();
//void vrapi_GLDebugindex(int index);
#if defined(__cplusplus)
} // extern "C"
#endif

#endif // OVR_VrApi_h
