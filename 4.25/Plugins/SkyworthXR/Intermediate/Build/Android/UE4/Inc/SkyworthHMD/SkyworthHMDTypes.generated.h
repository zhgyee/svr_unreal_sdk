// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKYWORTHHMD_SkyworthHMDTypes_generated_h
#error "SkyworthHMDTypes.generated.h already included, missing '#pragma once' in SkyworthHMDTypes.h"
#endif
#define SKYWORTHHMD_SkyworthHMDTypes_generated_h

#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDTypes_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSkyworthSplashDesc_Statics; \
	SKYWORTHHMD_API static class UScriptStruct* StaticStruct();


template<> SKYWORTHHMD_API UScriptStruct* StaticStruct<struct FSkyworthSplashDesc>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
