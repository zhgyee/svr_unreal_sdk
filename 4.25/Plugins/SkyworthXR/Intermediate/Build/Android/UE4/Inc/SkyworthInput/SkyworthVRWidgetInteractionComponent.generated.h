// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKYWORTHINPUT_SkyworthVRWidgetInteractionComponent_generated_h
#error "SkyworthVRWidgetInteractionComponent.generated.h already included, missing '#pragma once' in SkyworthVRWidgetInteractionComponent.h"
#endif
#define SKYWORTHINPUT_SkyworthVRWidgetInteractionComponent_generated_h

#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_SPARSE_DATA
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_RPC_WRAPPERS
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkyworthVRWidgetInteractionComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRWidgetInteractionComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRWidgetInteractionComponent, UWidgetInteractionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRWidgetInteractionComponent)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUSkyworthVRWidgetInteractionComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRWidgetInteractionComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRWidgetInteractionComponent, UWidgetInteractionComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRWidgetInteractionComponent)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthVRWidgetInteractionComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRWidgetInteractionComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRWidgetInteractionComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRWidgetInteractionComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRWidgetInteractionComponent(USkyworthVRWidgetInteractionComponent&&); \
	NO_API USkyworthVRWidgetInteractionComponent(const USkyworthVRWidgetInteractionComponent&); \
public:


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRWidgetInteractionComponent(USkyworthVRWidgetInteractionComponent&&); \
	NO_API USkyworthVRWidgetInteractionComponent(const USkyworthVRWidgetInteractionComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRWidgetInteractionComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRWidgetInteractionComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRWidgetInteractionComponent)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_PRIVATE_PROPERTY_OFFSET
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_8_PROLOG
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_PRIVATE_PROPERTY_OFFSET \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_SPARSE_DATA \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_RPC_WRAPPERS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_INCLASS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_PRIVATE_PROPERTY_OFFSET \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_SPARSE_DATA \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_INCLASS_NO_PURE_DECLS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKYWORTHINPUT_API UClass* StaticClass<class USkyworthVRWidgetInteractionComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRWidgetInteractionComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
