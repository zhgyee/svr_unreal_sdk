// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKYWORTHHMD_SkyworthSceneCaptureCubemap_generated_h
#error "SkyworthSceneCaptureCubemap.generated.h already included, missing '#pragma once' in SkyworthSceneCaptureCubemap.h"
#endif
#define SKYWORTHHMD_SkyworthSceneCaptureCubemap_generated_h

#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_SPARSE_DATA
#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_RPC_WRAPPERS
#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkyworthSceneCaptureCubemap(); \
	friend struct Z_Construct_UClass_USkyworthSceneCaptureCubemap_Statics; \
public: \
	DECLARE_CLASS(USkyworthSceneCaptureCubemap, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SkyworthHMD"), NO_API) \
	DECLARE_SERIALIZER(USkyworthSceneCaptureCubemap)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUSkyworthSceneCaptureCubemap(); \
	friend struct Z_Construct_UClass_USkyworthSceneCaptureCubemap_Statics; \
public: \
	DECLARE_CLASS(USkyworthSceneCaptureCubemap, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SkyworthHMD"), NO_API) \
	DECLARE_SERIALIZER(USkyworthSceneCaptureCubemap)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthSceneCaptureCubemap(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthSceneCaptureCubemap) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthSceneCaptureCubemap); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthSceneCaptureCubemap); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthSceneCaptureCubemap(USkyworthSceneCaptureCubemap&&); \
	NO_API USkyworthSceneCaptureCubemap(const USkyworthSceneCaptureCubemap&); \
public:


#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthSceneCaptureCubemap(USkyworthSceneCaptureCubemap&&); \
	NO_API USkyworthSceneCaptureCubemap(const USkyworthSceneCaptureCubemap&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthSceneCaptureCubemap); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthSceneCaptureCubemap); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USkyworthSceneCaptureCubemap)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CaptureComponents() { return STRUCT_OFFSET(USkyworthSceneCaptureCubemap, CaptureComponents); }


#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_16_PROLOG
#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_PRIVATE_PROPERTY_OFFSET \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_SPARSE_DATA \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_RPC_WRAPPERS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_INCLASS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_PRIVATE_PROPERTY_OFFSET \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_SPARSE_DATA \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_INCLASS_NO_PURE_DECLS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKYWORTHHMD_API UClass* StaticClass<class USkyworthSceneCaptureCubemap>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sdktest_Plugins_SkyworthXR_Source_SkyworthHMD_Private_SkyworthSceneCaptureCubemap_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
