// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKYWORTHINPUT_SkyworthVRLaserVisual_generated_h
#error "SkyworthVRLaserVisual.generated.h already included, missing '#pragma once' in SkyworthVRLaserVisual.h"
#endif
#define SKYWORTHINPUT_SkyworthVRLaserVisual_generated_h

#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_SPARSE_DATA
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_RPC_WRAPPERS
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkyworthVRLaserVisual(); \
	friend struct Z_Construct_UClass_USkyworthVRLaserVisual_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRLaserVisual, USceneComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRLaserVisual)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUSkyworthVRLaserVisual(); \
	friend struct Z_Construct_UClass_USkyworthVRLaserVisual_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRLaserVisual, USceneComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRLaserVisual)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthVRLaserVisual(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRLaserVisual) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRLaserVisual); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRLaserVisual); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRLaserVisual(USkyworthVRLaserVisual&&); \
	NO_API USkyworthVRLaserVisual(const USkyworthVRLaserVisual&); \
public:


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthVRLaserVisual(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRLaserVisual(USkyworthVRLaserVisual&&); \
	NO_API USkyworthVRLaserVisual(const USkyworthVRLaserVisual&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRLaserVisual); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRLaserVisual); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRLaserVisual)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_PRIVATE_PROPERTY_OFFSET
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_26_PROLOG
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_PRIVATE_PROPERTY_OFFSET \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_SPARSE_DATA \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_RPC_WRAPPERS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_INCLASS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_PRIVATE_PROPERTY_OFFSET \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_SPARSE_DATA \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_INCLASS_NO_PURE_DECLS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h_29_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SkyworthVRLaserVisual."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKYWORTHINPUT_API UClass* StaticClass<class USkyworthVRLaserVisual>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisual_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
