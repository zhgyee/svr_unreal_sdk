// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
class UMaterialInstanceDynamic;
class USkyworthVRLaserPlaneComponent;
class UMaterialBillboardComponent;
#ifdef SKYWORTHINPUT_SkyworthVRLaserVisualComponent_generated_h
#error "SkyworthVRLaserVisualComponent.generated.h already included, missing '#pragma once' in SkyworthVRLaserVisualComponent.h"
#endif
#define SKYWORTHINPUT_SkyworthVRLaserVisualComponent_generated_h

#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_SPARSE_DATA
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetPointerDistance); \
	DECLARE_FUNCTION(execGetLaserMaterial); \
	DECLARE_FUNCTION(execGetLaser); \
	DECLARE_FUNCTION(execGetReticle);


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetPointerDistance); \
	DECLARE_FUNCTION(execGetLaserMaterial); \
	DECLARE_FUNCTION(execGetLaser); \
	DECLARE_FUNCTION(execGetReticle);


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkyworthVRLaserVisualComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRLaserVisualComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRLaserVisualComponent, USkyworthVRLaserVisual, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRLaserVisualComponent)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUSkyworthVRLaserVisualComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRLaserVisualComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRLaserVisualComponent, USkyworthVRLaserVisual, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRLaserVisualComponent)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthVRLaserVisualComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRLaserVisualComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRLaserVisualComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRLaserVisualComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRLaserVisualComponent(USkyworthVRLaserVisualComponent&&); \
	NO_API USkyworthVRLaserVisualComponent(const USkyworthVRLaserVisualComponent&); \
public:


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRLaserVisualComponent(USkyworthVRLaserVisualComponent&&); \
	NO_API USkyworthVRLaserVisualComponent(const USkyworthVRLaserVisualComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRLaserVisualComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRLaserVisualComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USkyworthVRLaserVisualComponent)


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_PRIVATE_PROPERTY_OFFSET
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_25_PROLOG
#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_PRIVATE_PROPERTY_OFFSET \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_SPARSE_DATA \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_RPC_WRAPPERS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_INCLASS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_PRIVATE_PROPERTY_OFFSET \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_SPARSE_DATA \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_INCLASS_NO_PURE_DECLS \
	sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKYWORTHINPUT_API UClass* StaticClass<class USkyworthVRLaserVisualComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID sdktest_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRLaserVisualComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
