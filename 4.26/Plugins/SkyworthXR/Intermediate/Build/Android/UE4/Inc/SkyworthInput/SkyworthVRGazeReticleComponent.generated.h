// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKYWORTHINPUT_SkyworthVRGazeReticleComponent_generated_h
#error "SkyworthVRGazeReticleComponent.generated.h already included, missing '#pragma once' in SkyworthVRGazeReticleComponent.h"
#endif
#define SKYWORTHINPUT_SkyworthVRGazeReticleComponent_generated_h

#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_SPARSE_DATA
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_RPC_WRAPPERS
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkyworthVRGazeReticleComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRGazeReticleComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRGazeReticleComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRGazeReticleComponent) \
	virtual UObject* _getUObject() const override { return const_cast<USkyworthVRGazeReticleComponent*>(this); }


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUSkyworthVRGazeReticleComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRGazeReticleComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRGazeReticleComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRGazeReticleComponent) \
	virtual UObject* _getUObject() const override { return const_cast<USkyworthVRGazeReticleComponent*>(this); }


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthVRGazeReticleComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRGazeReticleComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRGazeReticleComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRGazeReticleComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRGazeReticleComponent(USkyworthVRGazeReticleComponent&&); \
	NO_API USkyworthVRGazeReticleComponent(const USkyworthVRGazeReticleComponent&); \
public:


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRGazeReticleComponent(USkyworthVRGazeReticleComponent&&); \
	NO_API USkyworthVRGazeReticleComponent(const USkyworthVRGazeReticleComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRGazeReticleComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRGazeReticleComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USkyworthVRGazeReticleComponent)


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_22_PROLOG
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_RPC_WRAPPERS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_INCLASS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKYWORTHINPUT_API UClass* StaticClass<class USkyworthVRGazeReticleComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRGazeReticleComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
