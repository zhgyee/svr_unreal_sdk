// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKYWORTHINPUT_SkyworthVRLaserPlaneComponent_generated_h
#error "SkyworthVRLaserPlaneComponent.generated.h already included, missing '#pragma once' in SkyworthVRLaserPlaneComponent.h"
#endif
#define SKYWORTHINPUT_SkyworthVRLaserPlaneComponent_generated_h

#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_SPARSE_DATA
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_RPC_WRAPPERS
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkyworthVRLaserPlaneComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRLaserPlaneComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRLaserPlaneComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRLaserPlaneComponent)


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUSkyworthVRLaserPlaneComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRLaserPlaneComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRLaserPlaneComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRLaserPlaneComponent)


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthVRLaserPlaneComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRLaserPlaneComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRLaserPlaneComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRLaserPlaneComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRLaserPlaneComponent(USkyworthVRLaserPlaneComponent&&); \
	NO_API USkyworthVRLaserPlaneComponent(const USkyworthVRLaserPlaneComponent&); \
public:


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRLaserPlaneComponent(USkyworthVRLaserPlaneComponent&&); \
	NO_API USkyworthVRLaserPlaneComponent(const USkyworthVRLaserPlaneComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRLaserPlaneComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRLaserPlaneComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USkyworthVRLaserPlaneComponent)


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_20_PROLOG
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_RPC_WRAPPERS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_INCLASS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKYWORTHINPUT_API UClass* StaticClass<class USkyworthVRLaserPlaneComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Private_SkyworthVRLaserPlaneComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
