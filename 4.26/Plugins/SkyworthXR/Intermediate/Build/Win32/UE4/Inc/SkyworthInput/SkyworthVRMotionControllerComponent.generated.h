// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMaterialInstanceDynamic;
class UStaticMeshComponent;
class UMotionControllerComponent;
#ifdef SKYWORTHINPUT_SkyworthVRMotionControllerComponent_generated_h
#error "SkyworthVRMotionControllerComponent.generated.h already included, missing '#pragma once' in SkyworthVRMotionControllerComponent.h"
#endif
#define SKYWORTHINPUT_SkyworthVRMotionControllerComponent_generated_h

#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_SPARSE_DATA
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentPointerDistance); \
	DECLARE_FUNCTION(execSetPointerDistance); \
	DECLARE_FUNCTION(execGetLaserMaterial); \
	DECLARE_FUNCTION(execGetControllerMesh); \
	DECLARE_FUNCTION(execGetMotionController);


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentPointerDistance); \
	DECLARE_FUNCTION(execSetPointerDistance); \
	DECLARE_FUNCTION(execGetLaserMaterial); \
	DECLARE_FUNCTION(execGetControllerMesh); \
	DECLARE_FUNCTION(execGetMotionController);


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkyworthVRMotionControllerComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRMotionControllerComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRMotionControllerComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRMotionControllerComponent) \
	virtual UObject* _getUObject() const override { return const_cast<USkyworthVRMotionControllerComponent*>(this); }


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_INCLASS \
private: \
	static void StaticRegisterNativesUSkyworthVRMotionControllerComponent(); \
	friend struct Z_Construct_UClass_USkyworthVRMotionControllerComponent_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRMotionControllerComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRMotionControllerComponent) \
	virtual UObject* _getUObject() const override { return const_cast<USkyworthVRMotionControllerComponent*>(this); }


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthVRMotionControllerComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRMotionControllerComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRMotionControllerComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRMotionControllerComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRMotionControllerComponent(USkyworthVRMotionControllerComponent&&); \
	NO_API USkyworthVRMotionControllerComponent(const USkyworthVRMotionControllerComponent&); \
public:


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRMotionControllerComponent(USkyworthVRMotionControllerComponent&&); \
	NO_API USkyworthVRMotionControllerComponent(const USkyworthVRMotionControllerComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRMotionControllerComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRMotionControllerComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USkyworthVRMotionControllerComponent)


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_38_PROLOG
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_RPC_WRAPPERS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_INCLASS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h_41_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKYWORTHINPUT_API UClass* StaticClass<class USkyworthVRMotionControllerComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRMotionControllerComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
