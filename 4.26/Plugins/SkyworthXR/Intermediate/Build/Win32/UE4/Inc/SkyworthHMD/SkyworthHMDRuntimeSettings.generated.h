// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKYWORTHHMD_SkyworthHMDRuntimeSettings_generated_h
#error "SkyworthHMDRuntimeSettings.generated.h already included, missing '#pragma once' in SkyworthHMDRuntimeSettings.h"
#endif
#define SKYWORTHHMD_SkyworthHMDRuntimeSettings_generated_h

#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_SPARSE_DATA
#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_RPC_WRAPPERS
#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkyworthHMDRuntimeSettings(); \
	friend struct Z_Construct_UClass_USkyworthHMDRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(USkyworthHMDRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthHMD"), NO_API) \
	DECLARE_SERIALIZER(USkyworthHMDRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUSkyworthHMDRuntimeSettings(); \
	friend struct Z_Construct_UClass_USkyworthHMDRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(USkyworthHMDRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SkyworthHMD"), NO_API) \
	DECLARE_SERIALIZER(USkyworthHMDRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthHMDRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthHMDRuntimeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthHMDRuntimeSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthHMDRuntimeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthHMDRuntimeSettings(USkyworthHMDRuntimeSettings&&); \
	NO_API USkyworthHMDRuntimeSettings(const USkyworthHMDRuntimeSettings&); \
public:


#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthHMDRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthHMDRuntimeSettings(USkyworthHMDRuntimeSettings&&); \
	NO_API USkyworthHMDRuntimeSettings(const USkyworthHMDRuntimeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthHMDRuntimeSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthHMDRuntimeSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthHMDRuntimeSettings)


#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_14_PROLOG
#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_RPC_WRAPPERS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_INCLASS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SkyworthHMDRuntimeSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKYWORTHHMD_API UClass* StaticClass<class USkyworthHMDRuntimeSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_SkyworthXR_Source_SkyworthHMD_Public_SkyworthHMDRuntimeSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
