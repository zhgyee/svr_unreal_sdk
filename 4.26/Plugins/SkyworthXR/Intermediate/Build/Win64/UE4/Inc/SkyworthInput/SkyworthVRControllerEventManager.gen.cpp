// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SkyworthInput/Classes/SkyworthVRControllerEventManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSkyworthVRControllerEventManager() {}
// Cross Module References
	SKYWORTHINPUT_API UFunction* Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_SkyworthInput();
	SKYWORTHINPUT_API UEnum* Z_Construct_UEnum_SkyworthInput_ESkyworthVRControllerState();
	SKYWORTHINPUT_API UFunction* Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerRecenterDelegate__DelegateSignature();
	SKYWORTHINPUT_API UClass* Z_Construct_UClass_USkyworthVRControllerEventManager_NoRegister();
	SKYWORTHINPUT_API UClass* Z_Construct_UClass_USkyworthVRControllerEventManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics
	{
		struct _Script_SkyworthInput_eventSkyworthVRControllerStateChangeDelegate_Parms
		{
			ESkyworthVRControllerState NewControllerState;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NewControllerState_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NewControllerState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::NewProp_NewControllerState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::NewProp_NewControllerState = { "NewControllerState", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_SkyworthInput_eventSkyworthVRControllerStateChangeDelegate_Parms, NewControllerState), Z_Construct_UEnum_SkyworthInput_ESkyworthVRControllerState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::NewProp_NewControllerState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::NewProp_NewControllerState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/SkyworthVRControllerEventManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_SkyworthInput, nullptr, "SkyworthVRControllerStateChangeDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_SkyworthInput_eventSkyworthVRControllerStateChangeDelegate_Parms), Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerRecenterDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerRecenterDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/SkyworthVRControllerEventManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerRecenterDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_SkyworthInput, nullptr, "SkyworthVRControllerRecenterDelegate__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerRecenterDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerRecenterDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerRecenterDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerRecenterDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* ESkyworthVRControllerState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SkyworthInput_ESkyworthVRControllerState, Z_Construct_UPackage__Script_SkyworthInput(), TEXT("ESkyworthVRControllerState"));
		}
		return Singleton;
	}
	template<> SKYWORTHINPUT_API UEnum* StaticEnum<ESkyworthVRControllerState>()
	{
		return ESkyworthVRControllerState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESkyworthVRControllerState(ESkyworthVRControllerState_StaticEnum, TEXT("/Script/SkyworthInput"), TEXT("ESkyworthVRControllerState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SkyworthInput_ESkyworthVRControllerState_Hash() { return 1704892565U; }
	UEnum* Z_Construct_UEnum_SkyworthInput_ESkyworthVRControllerState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SkyworthInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESkyworthVRControllerState"), 0, Get_Z_Construct_UEnum_SkyworthInput_ESkyworthVRControllerState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESkyworthVRControllerState::Disconnected", (int64)ESkyworthVRControllerState::Disconnected },
				{ "ESkyworthVRControllerState::Scanning", (int64)ESkyworthVRControllerState::Scanning },
				{ "ESkyworthVRControllerState::Connecting", (int64)ESkyworthVRControllerState::Connecting },
				{ "ESkyworthVRControllerState::Connected", (int64)ESkyworthVRControllerState::Connected },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Connected.Name", "ESkyworthVRControllerState::Connected" },
				{ "Connecting.Name", "ESkyworthVRControllerState::Connecting" },
				{ "Disconnected.Name", "ESkyworthVRControllerState::Disconnected" },
				{ "ModuleRelativePath", "Classes/SkyworthVRControllerEventManager.h" },
				{ "Scanning.Name", "ESkyworthVRControllerState::Scanning" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SkyworthInput,
				nullptr,
				"ESkyworthVRControllerState",
				"ESkyworthVRControllerState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void USkyworthVRControllerEventManager::StaticRegisterNativesUSkyworthVRControllerEventManager()
	{
	}
	UClass* Z_Construct_UClass_USkyworthVRControllerEventManager_NoRegister()
	{
		return USkyworthVRControllerEventManager::StaticClass();
	}
	struct Z_Construct_UClass_USkyworthVRControllerEventManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnControllerRecenteredDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnControllerRecenteredDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnControllerStateChangedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnControllerStateChangedDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_SkyworthInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * GoogleVRController Extensions Function Library\n */" },
		{ "IncludePath", "SkyworthVRControllerEventManager.h" },
		{ "ModuleRelativePath", "Classes/SkyworthVRControllerEventManager.h" },
		{ "ToolTip", "GoogleVRController Extensions Function Library" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate_MetaData[] = {
		{ "Comment", "/** DEPRECATED:  Please use VRNotificationsComponent's VRControllerRecentered delegate instead! */" },
		{ "ModuleRelativePath", "Classes/SkyworthVRControllerEventManager.h" },
		{ "ToolTip", "DEPRECATED:  Please use VRNotificationsComponent's VRControllerRecentered delegate instead!" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate = { "OnControllerRecenteredDelegate", nullptr, (EPropertyFlags)0x0010000030080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USkyworthVRControllerEventManager, OnControllerRecenteredDelegate_DEPRECATED), Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerRecenterDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate_MetaData[] = {
		{ "ModuleRelativePath", "Classes/SkyworthVRControllerEventManager.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate = { "OnControllerStateChangedDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USkyworthVRControllerEventManager, OnControllerStateChangedDelegate), Z_Construct_UDelegateFunction_SkyworthInput_SkyworthVRControllerStateChangeDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerRecenteredDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::NewProp_OnControllerStateChangedDelegate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USkyworthVRControllerEventManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::ClassParams = {
		&USkyworthVRControllerEventManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USkyworthVRControllerEventManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USkyworthVRControllerEventManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USkyworthVRControllerEventManager, 1684218256);
	template<> SKYWORTHINPUT_API UClass* StaticClass<USkyworthVRControllerEventManager>()
	{
		return USkyworthVRControllerEventManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USkyworthVRControllerEventManager(Z_Construct_UClass_USkyworthVRControllerEventManager, &USkyworthVRControllerEventManager::StaticClass, TEXT("/Script/SkyworthInput"), TEXT("USkyworthVRControllerEventManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USkyworthVRControllerEventManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
