// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SKYWORTHINPUT_SkyworthVRPointer_generated_h
#error "SkyworthVRPointer.generated.h already included, missing '#pragma once' in SkyworthVRPointer.h"
#endif
#define SKYWORTHINPUT_SkyworthVRPointer_generated_h

#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_SPARSE_DATA
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_RPC_WRAPPERS
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthVRPointer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRPointer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRPointer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRPointer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRPointer(USkyworthVRPointer&&); \
	NO_API USkyworthVRPointer(const USkyworthVRPointer&); \
public:


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkyworthVRPointer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkyworthVRPointer(USkyworthVRPointer&&); \
	NO_API USkyworthVRPointer(const USkyworthVRPointer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkyworthVRPointer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkyworthVRPointer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkyworthVRPointer)


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUSkyworthVRPointer(); \
	friend struct Z_Construct_UClass_USkyworthVRPointer_Statics; \
public: \
	DECLARE_CLASS(USkyworthVRPointer, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/SkyworthInput"), NO_API) \
	DECLARE_SERIALIZER(USkyworthVRPointer)


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_GENERATED_UINTERFACE_BODY() \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_GENERATED_UINTERFACE_BODY() \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~ISkyworthVRPointer() {} \
public: \
	typedef USkyworthVRPointer UClassType; \
	typedef ISkyworthVRPointer ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_INCLASS_IINTERFACE \
protected: \
	virtual ~ISkyworthVRPointer() {} \
public: \
	typedef USkyworthVRPointer UClassType; \
	typedef ISkyworthVRPointer ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_46_PROLOG
#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_54_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_RPC_WRAPPERS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_54_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_SPARSE_DATA \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h_49_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SKYWORTHINPUT_API UClass* StaticClass<class USkyworthVRPointer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_SkyworthXR_Source_SkyworthInput_Classes_SkyworthVRPointer_h


#define FOREACH_ENUM_ESKYWORTHVRPOINTERINPUTMODE(op) \
	op(ESkyworthVRPointerInputMode::Camera) \
	op(ESkyworthVRPointerInputMode::Direct) \
	op(ESkyworthVRPointerInputMode::HybridExperimental) 

enum class ESkyworthVRPointerInputMode : uint8;
template<> SKYWORTHINPUT_API UEnum* StaticEnum<ESkyworthVRPointerInputMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
