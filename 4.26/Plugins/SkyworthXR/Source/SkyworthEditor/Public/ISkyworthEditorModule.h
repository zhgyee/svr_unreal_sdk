/*
 * @Author: xieminghui
 * @Date: 2021-03-19 15:43:32
 * @Description: Description
 * @LastEditors: xieminghui
 * @LastEditTime: 2021-03-19 15:44:00
 * @Copyright: Copyright 2020 Skyworth VR. All rights reserved.
 */
// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"

class FToolBarBuilder;
class FMenuBuilder;

#define SKYWORTH_EDITOR_MODULE_NAME "SkyworthEditor"

//////////////////////////////////////////////////////////////////////////
// ISkyworthEditorModule

class ISkyworthEditorModule : public IModuleInterface
{
};

